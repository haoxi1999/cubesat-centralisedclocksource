#include "Arduino.h"

int T_SDA;
int T_SCL;

void init_getTimeStamp(int set_T_SDA, int set_T_SCL) {
  //init
  T_SDA = set_T_SDA;
  T_SCL = set_T_SCL;
  pinMode(T_SDA, INPUT);
  pinMode(T_SCL, INPUT);;
}

int getTimeStamp(byte* data, int len) { 
  //int timeout = 0;
  //wait for end of transmission
  while(1) {
    if(digitalRead(T_SDA) == HIGH && digitalRead(T_SCL) == HIGH) {
      delayMicroseconds(1);
      if(digitalRead(T_SDA) == LOW && digitalRead(T_SCL) == HIGH){ 
        break;
      }
    } 
  }
  //Serial.write("recieved end of transmission\n");
  byte checkSum = 0x00;
  for (int byteCount = 0; byteCount <= len; byteCount++) {
    data[byteCount] = 0x00;
    if(byteCount < len) {
      for (int bitCount = 7; bitCount >= 0; bitCount--) {
        while(digitalRead(T_SCL) == HIGH){};
        delayMicroseconds(1);
        while(digitalRead(T_SCL) == LOW){};
        delayMicroseconds(1);
        //vote
        if(digitalRead(T_SDA) == HIGH ? 1 : 0)
          data[byteCount] += 0x01;
        if(bitCount != 0)
          data[byteCount] <<= 1;
      }
      checkSum += data[byteCount];
    } else {
      byte sum = 0x00;
      for (int bitCount = 7; bitCount >= 0; bitCount--) {
        while(digitalRead(T_SCL) == HIGH){};
        delayMicroseconds(1);
        while(digitalRead(T_SCL) == LOW){};
        delayMicroseconds(1);
        //vote
        if(digitalRead(T_SDA) == HIGH ? 1 : 0)
          sum += 0x01;
        if(bitCount != 0)
          sum <<= 1;
      }
      if(checkSum != sum ) {
          return 1;
        }
    }
    //Serial.write(data[byteCount]);
  }
  return 0;
}
