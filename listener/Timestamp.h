#include "Arduino.h"

#ifndef TIME_STAMP_INCLUDED
#define TIME_STAMP_INCLUDED

void init_getTimeStamp(int ,int); //initialize the broadcast listener, arguments are two pin numbers: T_SDA, T_SCL
int getTimeStamp(byte*, int); //get n bytes, return 0 if the acquired data passes the checksum test, the data is NOT trustworthy if checksum fails. argument list: buffer*, buffer_size

#endif